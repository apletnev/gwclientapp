import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ITour} from "../../models/tours";
import {IOrder} from "../../models/order";

@Injectable({
  providedIn: 'root'
})

export class TourRestService {

  constructor(private http: HttpClient) { }

  getTours(): Observable<ITour[]> {
    return this.http.get<ITour[]>('http://localhost:3000/tours/');
  }

  sendTourData(data: IOrder): Observable<any> {
    return this.http.post('http://localhost:3000/order/', data)
  }

  getTourById(id: string): Observable<ITour> {
    return this.http.get<ITour>('http://localhost:3000/tours/' + id);
  }

  createTour(body: any): Observable<any> {
    return this.http.post('http://localhost:3000/tour-item', body, {headers:{}});
  }

  removeTourById(id: string){
    return this.http.delete("http://localhost:3000/tours/" + id);
  }

}
