import { TestBed } from '@angular/core/testing';

import { TourRestService } from './tour-rest.service';

describe('TourRestService', () => {
  let service: TourRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TourRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
