import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {ISettings} from "../../models/settings";

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private settingSubject: Subject<ISettings> = new Subject<ISettings>();

  constructor() { }

  loadUserSettings(): Observable<ISettings> {
    const settingsObservable = new Observable<ISettings>(
      (subscriber) => {
        const settingsData:ISettings = {
          saveToken: true
        };
        subscriber.next(settingsData);
      }
    )
    return settingsObservable;
  }

  loadUserSettingsSubject(data: ISettings): any {
    this.settingSubject.next(data);
  }

  getSettingsSubjectObservable(): Observable<ISettings>{
    return this.settingSubject.asObservable();
  }

}
