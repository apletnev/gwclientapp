import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import {ITourTypeSelect} from "../../models/tours";

@Injectable({
  providedIn: 'root'
})
export class AsideService {

  constructor() { }

  public asideSubject$ = new Subject<boolean>();

  updateAside(isAsideOn: boolean): void {
    this.asideSubject$.next(isAsideOn);
  }

}
