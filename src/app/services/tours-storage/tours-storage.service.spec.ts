import { TestBed } from '@angular/core/testing';

import { ToursStorageService } from './tours-storage.service';

describe('ToursStorageService', () => {
  let service: ToursStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ToursStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
