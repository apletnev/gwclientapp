import { Injectable } from '@angular/core';
import {ITour} from "../../models/tours";

@Injectable({
  providedIn: 'root'
})

export class ToursStorageService {

  private toursStorage: ITour[];

  constructor() { }

  setStorage(data: ITour[]): void {
    this.toursStorage = data;
  }

  getStorage(): ITour[] {
    return this.toursStorage;
  }
}
