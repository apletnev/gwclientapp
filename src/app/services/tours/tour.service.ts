import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {ITour, ITourTypeSelect} from "../../models/tours";
import {TourRestService} from "../rest/tour-rest.service";

@Injectable({
  providedIn: 'root'
})
export class TourService {

  private tourSubject = new Subject<ITourTypeSelect>();

  private tourUpdateSubject = new Subject<ITour[]>();
  readonly tourUpdateSubject$ = this.tourUpdateSubject.asObservable();

  //понадобится для отбора по типу тура
  readonly tourType$ = this.tourSubject.asObservable();

  constructor(private tourRestService: TourRestService) {
  }

  getTours(): Observable<ITour[]> {
    return this.tourRestService.getTours();
  }

  updateTour(type:ITourTypeSelect): void {
    this.tourSubject.next(type);
  }

  sendTourData(data: any): Observable<any> {
    return this.tourRestService.sendTourData(data);
  }

  updateTourList(data: ITour[]) {
    this.tourUpdateSubject.next(data);
  }

  getTourById(id:string): Observable<ITour>{
    return this.tourRestService.getTourById(id);
  }

  createTour(body: any) {
    return  this.tourRestService.createTour(body);
  }

  removeTourById(id: string){
    return this.tourRestService.removeTourById(id);
  }

}
