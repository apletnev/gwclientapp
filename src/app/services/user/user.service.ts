import { Injectable } from '@angular/core';
import {IUser} from "../../models/users";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user: IUser | null;
  private token: string | null;

  constructor() { }

  getUser(): IUser | null{
    return this.user;
  }

  setUser(user: IUser): void{
    this.user = user;
  }

  setToken(token: string): void {
    this.token = token;
  }

  setTokenInStorage(token: string): void {
    localStorage.setItem('userToken', token);
  }

  setUserInStorage(authUser: IUser): void {
    localStorage.setItem('authUser', JSON.stringify(authUser));
  }

  removeTokenInStorage(): void {
    localStorage.removeItem('userToken');
  }

  removeUserInStorage(): void {
    localStorage.removeItem('authUser');
  }

  getToken(): string | null {
    return this.token;
  }

}
