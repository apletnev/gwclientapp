export interface IOrder {
  firstName:   string | null,
  lastName:    string | null,
  email:       string | null,
  phoneNumber: string | null,
  cardNumber: string | null,
  tourName: string | null,
  tourId: string| null ,
  userId: string | null,
}
