export interface IConfig {
  adminKeyword: string;
  serverProtocol: string,
  baseIndexHref: string,
  useUserCard: boolean
}
