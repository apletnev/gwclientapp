export interface ITour {
  name:string,
  description:string,
  img:string,
  id:string,
  _id:string,
  type: string
}

export interface ITourTypeSelect {
  label?: string,
  value?: string
}
