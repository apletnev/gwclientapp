export interface IUser {
  login: string,
  psw: string,
  newPsw?: string,
  rptNewPsw?: string
  phoneNumber?:string;
  email?: string,
  firstName?: string,
  lastName?: string,
  cardNumber?: string,
  id?:string
  isAdmin: boolean
}
