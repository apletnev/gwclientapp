import {Component, OnInit} from '@angular/core';
import {ConfigService} from "./services/config/config.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'tourCatalogSPB';
  prop: string;

  constructor(private config: ConfigService) {
  }

  ngOnInit() {

    this.config.configLoad();

  }
}
