import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {OrderService} from "../../services/order/order.service";
import {TourService} from "../../services/tours/tour.service";
import {take} from "rxjs";
import {AsideService} from "../../services/aside/aside.service";


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit {

  orders: any;

  constructor(private orderService: OrderService,
              private userService: UserService,
              private asideService: AsideService) { }

  ngOnInit(): void {

    this.updateOrdersList();

  }

  deleteOrder(order: any) {

    this.orderService.removeOrderById(order._id).subscribe((data)=>{
      this.updateOrdersList();
    });

  }

  updateOrdersList() {

    if (this.userService.getUser()?.isAdmin) {
      this.orderService.getAllOrders().subscribe((data) => {
        this.orders = data;
      })
    } else {
      const userId = <string>this.userService.getUser()?.id;
      this.orderService.getOrders(userId).subscribe((data) => {
        this.orders = data;
      })
    }
  }

}
