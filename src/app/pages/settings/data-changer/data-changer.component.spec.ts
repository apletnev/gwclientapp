import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataChangerComponent } from './data-changer.component';

describe('DataChangerComponent', () => {
  let component: DataChangerComponent;
  let fixture: ComponentFixture<DataChangerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataChangerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataChangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
