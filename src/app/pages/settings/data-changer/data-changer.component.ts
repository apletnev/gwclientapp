import { Component, OnInit } from '@angular/core';
import {MessageService} from "primeng/api";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ConfigService} from "../../../services/config/config.service";
import {UserService} from "../../../services/user/user.service";
import {IUser} from "../../../models/users";
import {ServerError} from "../../../models/error";

@Component({
  selector: 'app-data-changer',
  templateUrl: './data-changer.component.html',
  styleUrls: ['./data-changer.component.scss']
})
export class DataChangerComponent implements OnInit {

  firstName: string | undefined;
  lastName: string | undefined;
  phoneNumber: string | undefined;
  email: string | undefined;
  cardNumber: string | undefined;
  showCardNumber: boolean;

  constructor(private messageService: MessageService,
              private http: HttpClient,
              private userService: UserService) { }

  ngOnInit(): void {

    this.showCardNumber = ConfigService.config.useUserCard;

    const user = <IUser>this.userService.getUser();

    this.firstName    = user?.firstName;
    this.lastName     = user?.lastName;
    this.phoneNumber  = user?.phoneNumber;
    this.email        = user?.email;
    this.cardNumber   = user?.cardNumber;

  }

  changeData($event: MouseEvent) {

    const user = <IUser>this.userService.getUser();

    user.firstName    = this.firstName;
    user.lastName     = this.lastName;
    user.phoneNumber  = this.phoneNumber;
    user.email        = this.email;
    user.cardNumber   = this.cardNumber

    this.userService.setUser(user);

    this.http.put<IUser>('http://localhost:3000/users/' + user.id + '', user)
      .subscribe((data) => {

        this.messageService.add({severity: 'success', summary: 'Данные обновлены.'});

      }, (err: HttpErrorResponse)=> {

        const serverError = <ServerError>err.error;
        this.messageService.add({severity:'warn', summary: 'Ошибка при обновлении данных. ' + serverError.errorText});

      });
  }
}
