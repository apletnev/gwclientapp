import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {SettingsService} from "../../../services/settings/settings.service";
import {MessageService} from "primeng/api";
import {UserService} from "../../../services/user/user.service";
import {IUser} from "../../../models/users";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "../../../models/error";
import * as bcrypt from "bcryptjs";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit, OnDestroy {

  subjectForUnsubscribe = new Subject();

  user: IUser | null;
  newPsw: string;
  rptNewPsw: string;
  pswUser: string;
  showTourLoader: boolean;

  constructor(private settingsService: SettingsService,
              private messageService: MessageService,
              private userService: UserService,
              private http: HttpClient) { }

  ngOnInit(): void {

    if (this.userService.getUser()?.isAdmin === null){
      this.showTourLoader = false;
    } else {
      this.showTourLoader = <boolean>this.userService.getUser()?.isAdmin;
    }

    this.settingsService.loadUserSettings()
                        .pipe(takeUntil(this.subjectForUnsubscribe))
                        .subscribe(
      (data) => {
      });

    this.settingsService.getSettingsSubjectObservable()
                                                .pipe(takeUntil(this.subjectForUnsubscribe))
                                                .subscribe(
        (data) => {
        }
      )

  }

  ngOnDestroy(): void {

    this.subjectForUnsubscribe.next(true);
    this.subjectForUnsubscribe.complete();

  }

  onPswChange(Ev: Event): void | boolean {

    const user = <IUser>this.userService.getUser();

    const isPswMatched = bcrypt.compareSync(this.pswUser, user.psw);

    if (!isPswMatched) { //( user.psw !== this.pswUser) {

      this.messageService.add({severity: 'error', summary: 'Неверно введен текущий пароль'});

    } else {

      if (this.newPsw !== this.rptNewPsw) {

        this.messageService.add({severity: 'error', summary: 'Пароли не совпадают'});

      } else {

        const salt = bcrypt.genSaltSync(10);
        const hashPsw = bcrypt.hashSync(this.newPsw, salt);

        user.psw = hashPsw;
        this.userService.setUser(user);

        this.http.put<IUser>('http://localhost:3000/users/' + user.id + '', user)
          .subscribe((data) => {

            this.messageService.add({severity: 'success', summary: 'Новый пароль установлен'});

          }, (err: HttpErrorResponse)=> {

            const serverError = <ServerError>err.error;
            this.messageService.add({severity:'warn', summary: 'Ошибка при установке пароля. ' + serverError.errorText});

          });

      }
    }
  }

}
