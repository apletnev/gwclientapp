import { Component, OnInit } from '@angular/core';
import {TourService} from "../../../services/tours/tour.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ITourTypeSelect} from "../../../models/tours";
import {MessageService, SelectItem} from "primeng/api";
import {ServerError} from "../../../models/error";

@Component({
  selector: 'app-tour-loader',
  templateUrl: './tour-loader.component.html',
  styleUrls: ['./tour-loader.component.scss']
})
export class TourLoaderComponent implements OnInit {

  tourForm: FormGroup;

  tourTypes: SelectItem[];

  constructor(private tourService: TourService,
              private http: HttpClient,
              private messageService: MessageService) { }

  ngOnInit(): void {
    this.tourForm = new FormGroup({
        name:        new FormControl('', [Validators.required, Validators.minLength(2)]),
        description: new FormControl('', [Validators.required, Validators.minLength(2)]),
        type:        new FormControl('single', {validators: Validators.required}),
        img:         new FormControl()
    });
    this.tourTypes = [
      {label: 'Индивидуальная (до 3 человек)', value: 'single'},
      {label: 'Совместная (группа до 15 человек)', value: 'multi'}
    ]
  }

  createTour(): void {

    const tourDataRaw = this.tourForm.getRawValue();

    let formParams = new FormData();
    if (typeof tourDataRaw === 'object') {
      for (let prop in tourDataRaw){
        formParams.append(prop, tourDataRaw[prop])
      }
    }
    this.tourService.createTour(formParams).subscribe(
      (data)=>{
          this.messageService.add({severity:'success', summary:'Экскурсия создана'});
        },
      (err: HttpErrorResponse)=> {
        const serverError = <ServerError>err.error;
        this.messageService.add({severity:'warn', summary:"Ошибка при загрузке:" + serverError.errorText});
      })
  }

  selectFile(ev: any): void {
    if (ev.target.files.length > 0){
      const file = ev.target.files[0];
      this.tourForm.patchValue({
        img: file
      });
    }
  }

  deleteTours():void {
    this.http.delete("http://localhost:3000/tours/").subscribe((data)=>{
      this.tourService.updateTourList([]);
    });
  }

}
