import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings/settings.component';
import {TabViewModule} from "primeng/tabview";
import {InputTextModule} from "primeng/inputtext";
import {ToastModule} from "primeng/toast";
import {ButtonModule} from "primeng/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TableModule} from "primeng/table";
import { TourLoaderComponent } from './tour-loader/tour-loader.component';
import { DataChangerComponent } from './data-changer/data-changer.component';
import {DropdownModule} from "primeng/dropdown";


@NgModule({
  declarations: [
    SettingsComponent,
    TourLoaderComponent,
    DataChangerComponent],
    imports: [
        CommonModule,
        SettingsRoutingModule,
        TabViewModule,
        InputTextModule,
        ToastModule,
        ButtonModule,
        FormsModule,
        TableModule,
        ReactiveFormsModule,
        DropdownModule
    ]
})
export class SettingsModule { }
