import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TourInfoRoutingModule } from './tour-info-routing.module';
import { TourItemComponent } from './tour-item/tour-item.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {InputNumberModule} from "primeng/inputnumber";
import {CalendarModule} from "primeng/calendar";
import {CarouselModule} from "primeng/carousel";
import {ToastModule} from "primeng/toast";

@NgModule({
  declarations: [
    TourItemComponent
  ],
    imports: [
        CommonModule,
        TourInfoRoutingModule,
        ReactiveFormsModule,
        InputTextModule,
        InputNumberModule,
        CalendarModule,
        CarouselModule,
        FormsModule,
        ToastModule
    ]
})
export class TourInfoModule { }
