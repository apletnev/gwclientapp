import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ITour} from "../../../models/tours";
import {ActivatedRoute, Router} from "@angular/router";
import {ToursStorageService} from "../../../services/tours-storage/tours-storage.service";
import {IUser} from "../../../models/users";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user/user.service";
import {IOrder} from "../../../models/order";
import {forkJoin, fromEvent, Subscription} from "rxjs";
import {TourService} from "../../../services/tours/tour.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "../../../models/error";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-tour-item',
  templateUrl: './tour-item.component.html',
  styleUrls: ['./tour-item.component.scss']
})
export class TourItemComponent implements OnInit {

  tour:     ITour | undefined;
  user:     IUser | null;
  userForm: FormGroup;
  isSendingOrderDisabled:boolean;

  constructor(private route: ActivatedRoute,
              private tourStorage: ToursStorageService,
              private userService: UserService,
              private tourService: TourService,
              private messageService: MessageService,
              private router: Router
              ) { }

  ngOnInit(): void {

    this.isSendingOrderDisabled = false;

    this.user = this.userService.getUser();

    this.userForm = new FormGroup({
      firstName:   new FormControl("", {validators: Validators.required}),
      lastName:    new FormControl("", [Validators.required, Validators.minLength(2)]),
      email:       new FormControl(),
      phoneNumber: new FormControl(),
      cardNumber:  new FormControl()//this.user?.cardNumber),
    });

    this.userForm.controls["firstName"].setValue(this.user?.firstName);
    this.userForm.controls["lastName"].setValue(this.user?.lastName);
    this.userForm.controls["email"].setValue(this.user?.email);
    this.userForm.controls["phoneNumber"].setValue(this.user?.phoneNumber);
    this.userForm.controls["cardNumber"].setValue(this.user?.cardNumber);

    const routeIdParam = this.route.snapshot.paramMap.get('id');
    const queryIdParam = this.route.snapshot.queryParamMap.get('id');

    const paramValueId = routeIdParam || queryIdParam;
    if (paramValueId) {
      const tourStorage = this.tourStorage.getStorage();
      this.tour = tourStorage.find((el)=> el._id === paramValueId);
    }
  }

  ngAfterViewInit(): void {

  }

  initTour(): void {

    const userData = this.userForm.getRawValue();
    const postData = {...this.tour, ...userData};

    const userId = this.userService.getUser()?.id || null ;

    const sType = (postData.type==='single')?'индивидуальная':'совместная';
    const tourNameCon = postData.name + ", " + sType;

    const postObj: IOrder = {
      firstName:   postData.firstName,
      lastName:    postData.lastName,
      email:       postData.email,
      phoneNumber: postData.phoneNumber,
      cardNumber:  postData.cardNumber,
      tourName:    tourNameCon,
      tourId:      postData._id,
      userId:      userId
    }

    this.tourService.sendTourData(postObj).subscribe({
      next: value => {
        this.messageService.add({severity:'success', summary:'Заказ отправлен'});
        this.isSendingOrderDisabled = true;
        },
      error: err => {
        const serverError = <ServerError>err.error;
        this.messageService.add({severity:'warn', summary:"Ошибка при отправке заказа." + serverError.errorText});
      }
    });

  }

}
