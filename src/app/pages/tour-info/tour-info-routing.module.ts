import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TourItemComponent} from "./tour-item/tour-item.component";

const routes: Routes = [
  {
    path: '',
    component: TourItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TourInfoRoutingModule { }
