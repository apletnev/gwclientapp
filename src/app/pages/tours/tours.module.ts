import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToursRoutingModule } from './tours-routing.module';
import { ToursComponent } from './tours.component';
import {MessageService} from "primeng/api";
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TourListComponent } from './tour-list/tour-list.component';
import { AsideComponent } from './aside/aside.component';
import {MenubarModule} from "primeng/menubar";
import {DropdownModule} from "primeng/dropdown";
import {FormsModule} from "@angular/forms";
import {CalendarModule} from "primeng/calendar";
import {InputTextModule} from "primeng/inputtext";



@NgModule({
  declarations: [
    ToursComponent,
    HeaderComponent,
    FooterComponent,
    TourListComponent,
    AsideComponent
  ],
  imports: [
    CommonModule,
    ToursRoutingModule,
    MenubarModule,
    DropdownModule,
    FormsModule,
    CalendarModule,
    InputTextModule
  ],
  providers: [MessageService]
}) export class ToursModule { }



