import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ToursComponent} from "./tours.component";
import {TourListComponent} from "./tour-list/tour-list.component";

const routes: Routes = [
  { path: '',
    component: ToursComponent,
    children: [
      {
        path: 'tours-list',
        component: TourListComponent
      },
      {
        //path: 'tours/:id',
        path: 'tour',
        loadChildren: () => import('../tour-info/tour-info.module').then(m => m.TourInfoModule)
      },
      {
        path: 'order',
        loadChildren: () => import('../order/order.module').then(m => m.OrderModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule)
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToursRoutingModule { }
