import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ITour, ITourTypeSelect} from "../../../models/tours";
import {TourService} from "../../../services/tours/tour.service";
import {MessageService} from "primeng/api";
import {SettingsService} from "../../../services/settings/settings.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  tourTypes: ITourTypeSelect[] = [
    {label: 'Все', value: 'all'},
    {label: 'Индивидуальная (до 3 человек)', value: 'single'},
    {label: 'Совместная (группа до 15 человек)', value: 'multi'}
  ]

  constructor(private tourService: TourService,
              private messageService: MessageService,
              private settingsService: SettingsService,
              private http: HttpClient) { }

  ngOnInit(): void {

  }

  changeTourType(ev:  {ev: Event, value: ITourTypeSelect}): void {
    this.tourService.updateTour(ev.value)
  }

}
