import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TourService} from "../../../services/tours/tour.service";
import {ITour, ITourTypeSelect} from "../../../models/tours";
import {ToursStorageService} from "../../../services/tours-storage/tours-storage.service";
import {Router} from "@angular/router";
import {debounceTime, fromEvent, Subscription} from "rxjs";
import {AsideService} from "../../../services/aside/aside.service";
import {UserService} from "../../../services/user/user.service";

@Component({
  selector: 'app-tour-list',
  templateUrl: './tour-list.component.html',
  styleUrls: ['./tour-list.component.scss']
})
export class TourListComponent implements OnInit, AfterViewInit, OnDestroy {

  tours:     ITour[];
  toursCopy: ITour[];
  defaultDate: string;
  isAdmin: boolean | undefined;

  tourUnsubscriber: Subscription;

  @ViewChild('tourSearch') tourSearch: ElementRef;

  searchTourSub: Subscription;
  tourSearchValue: string;

  constructor(private tourService: TourService,
              private tourStorage: ToursStorageService,
              private router: Router,
              private asideService: AsideService,
              private userService: UserService) { }

  ngOnInit(): void {

    this.isAdmin = this.userService.getUser()?.isAdmin;
    this.updateToursList();

  }

  updateToursList(){

    this.tourService.tourUpdateSubject$.subscribe((data)=>{
      this.tours = data;
    })

    this.tourService.getTours().subscribe(
      (data) => {
        this.tours = data;
        this.toursCopy = [...this.tours];
        //заполняем, чтобы потом использовать в tour-item
        this.tourStorage.setStorage(data);
        this.asideService.updateAside(true);
      }
    )

    this.tourUnsubscriber = this.tourService.tourType$.subscribe((data:ITourTypeSelect) => {
      switch (data.value) {
        case "single":
          this.tours = this.toursCopy.filter((el) => el.type === "single");
          break;
        case "multi":
          this.tours = this.toursCopy.filter((el) => el.type === "multi");
          break;
        case "all":
          this.tours = [...this.toursCopy];
          break;
      }
    });

  }

  ngAfterViewInit() {

    const fromEventObserver = fromEvent(this.tourSearch.nativeElement,
                              'keyup',
                      {passive: true});

    this.searchTourSub = fromEventObserver
                            .pipe(debounceTime(200))
                            .subscribe((ev)=>{
                              if (this.tourSearchValue) {
                                this.tours = this.toursCopy.filter((el)=> el.name.toLowerCase().includes(this.tourSearchValue.toLowerCase()));
                              } else {
                                this.tours = [...this.toursCopy];
                              }
                            });

  }

  goToTourInfoPage(item: ITour) {
    this.router.navigate(['/tours/tour'],{queryParams:{id: item._id}});
  }

  ngOnDestroy() {
    this.tourUnsubscriber.unsubscribe();
    this.searchTourSub.unsubscribe();
    this.asideService.updateAside(false);
  }

  deleteTour(item: ITour) {
    this.tourService.removeTourById(item._id).subscribe(()=>{
      this.updateToursList();
    });
  }
}
