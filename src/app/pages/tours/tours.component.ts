import {Component, OnDestroy, OnInit} from '@angular/core';
import {ITourTypeSelect} from "../../models/tours";
import {Subscription} from "rxjs";
import {TourService} from "../../services/tours/tour.service";
import {ToursStorageService} from "../../services/tours-storage/tours-storage.service";
import {Router} from "@angular/router";
import {AsideService} from "../../services/aside/aside.service";

@Component({
  selector: 'app-tours',
  templateUrl: './tours.component.html',
  styleUrls: ['./tours.component.scss']
})

export class ToursComponent implements OnInit, OnDestroy {

  isAsideOn: boolean = true;
  asideSubscription: Subscription;

  constructor(private asideService: AsideService) { }

  ngOnInit(): void {

    this.asideSubscription = this.asideService.asideSubject$.subscribe((data:boolean) => {
      this.isAsideOn = data;
    });

  }

  ngOnDestroy() {
    this.asideSubscription.unsubscribe();
  }

}
