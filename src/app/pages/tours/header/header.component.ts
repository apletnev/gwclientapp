import {Component, Input, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {MenuItem} from "primeng/api";
import {UserService} from "../../../services/user/user.service";
import {IUser} from "../../../models/users";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  items: MenuItem[];
  time: Date;
  private timerInterval: number;
  user: IUser | null;
  private settingsActive = true;
  private labelForOrders: string;

  constructor(private userService: UserService) { }

  ngOnInit(): void {

    this.user = this.userService.getUser();
    this.labelForOrders = "Мои заказы";
    if (this.user?.isAdmin){
      this.labelForOrders = "Все заказы";
    }

    this.items = this.initMenuItems();

    this.timerInterval = window.setInterval(() => {
      this.time = new Date();
    }, 1000);

  }

  ngOnDestroy(): void {
    if (this.timerInterval) {
      window.clearInterval(this.timerInterval);
    }
  }

  initMenuItems(): MenuItem[] {
    return [
      {
        label: 'Экскурсии',
        routerLink:['tours-list']
      },
      {
        label: this.labelForOrders,
        routerLink: ['order']
      },
      {
        label: 'Настройки',
        routerLink:['settings'],
        visible: this.settingsActive
      },
      {
        label: 'Выйти',
        routerLink:['/auth']
      },

    ];
  }

}
