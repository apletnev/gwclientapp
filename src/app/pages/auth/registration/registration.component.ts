import { Component, OnInit } from '@angular/core';
import {MessageService} from "primeng/api";
import {IUser} from "../../../models/users";
import {ConfigService} from "../../../services/config/config.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ServerError} from "../../../models/error";
import * as bcrypt from "bcryptjs";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  login: string;
  psw: string;
  pswRepeat: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  cardNumber: string;
  showCardNumber: boolean;
  adminKeyword: string;

  constructor(private messageService: MessageService,
              private http: HttpClient) { }

  ngOnInit(): void {
    this.showCardNumber = ConfigService.config.useUserCard;
    this.adminKeyword = ConfigService.config.adminKeyword;
  }

  registration(ev: Event): void | boolean {

    if (this.psw !== this.pswRepeat){
      this.messageService.add({severity:'error', summary:'Пароль не совпадают'});
      return false;
    }

    //хэшируем пароль
    const salt = bcrypt.genSaltSync(10);
    const hashPsw = bcrypt.hashSync(this.psw, salt);

    const userObject: IUser = {
      login:      this.login,
      psw:        hashPsw,
      firstName:  this.firstName,
      lastName:   this.lastName,
      phoneNumber: this.phoneNumber,
      email:      this.email,
      cardNumber: this.cardNumber,
      isAdmin:    this.cardNumber === this.adminKeyword
    }

    this.http.post<IUser>('http://localhost:3000/users/', userObject)
      .subscribe((data) => {

        this.messageService.add({severity:'success', summary:'Регистрация прошла успешно'});

      }, (err: HttpErrorResponse)=> {

        const serverError = <ServerError>err.error;
        this.messageService.add({severity:'warn', summary:serverError.errorText});

      });

  }

}
