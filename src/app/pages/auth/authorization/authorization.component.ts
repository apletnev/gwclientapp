import {Component, OnDestroy, OnInit} from '@angular/core';
import {IUser} from "../../../models/users";
import {MessageService} from "primeng/api";
import {Router, ActivatedRoute} from "@angular/router";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {UserService} from "../../../services/user/user.service";
import {ConfigService} from "../../../services/config/config.service";
import {ServerError} from "../../../models/error";
import * as bcrypt from "bcryptjs";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})

export class AuthorizationComponent implements OnInit, OnDestroy {
  loginText = 'Логин';
  pswText   = 'Пароль';

  login: string;
  psw: string;
  firstName: string | undefined;
  lastName:  string | undefined;
  phoneNumber: string | undefined;
  email:       string | undefined;
  cardNumber:  string | undefined;
  authTextButton: string;
  showCardNumber: boolean;
  isAdmin: boolean;

  constructor(private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private http: HttpClient)
              { }

  ngOnInit(): void {
    this.authTextButton = 'Войти';
    this.showCardNumber = ConfigService.config.useUserCard;
  }

  ngOnDestroy(){
  }

  onAuth(ev: Event): void {

    const authUser: IUser = {
      psw:          this.psw,
      login:        this.login,
      firstName:    this.firstName,
      lastName:     this.lastName,
      phoneNumber:  this.phoneNumber,
      email:        this.email,
      cardNumber:   this.cardNumber,
      isAdmin:      this.isAdmin
    }

    this.http.post<{access_token: string,
                    id:           string,
                    firstName:    string,
                    lastName:     string,
                    phoneNumber:  string,
                    email:        string,
                    cardNumber:   string,
                    isAdmin:      boolean}>
      ('http://localhost:3000/users/' + authUser.login, authUser).subscribe((data) => {


      authUser.id      = data.id;
      authUser.isAdmin = data.isAdmin;
      authUser.firstName    = data.firstName;
      authUser.lastName     = data.lastName;
      authUser.phoneNumber  = data.phoneNumber;
      authUser.email        = data.email;
      authUser.cardNumber   = data.cardNumber

      this.userService.setUser(authUser);

      const token: string = data.access_token;
      this.userService.setToken(token);

      const salt = bcrypt.genSaltSync(10);
      authUser.psw = bcrypt.hashSync(authUser.psw, salt);

      this.userService.setUserInStorage(authUser);
      this.userService.setTokenInStorage(token);

      this.router.navigate(['tours/tours-list'])

    }, (err: HttpErrorResponse) => {

      //const serverError = <ServerError>err.error;
      this.messageService.add({severity: 'warn', summary: 'Ошибка авторизации. Проверьте логин и пароль.'});

    });


  }

}
